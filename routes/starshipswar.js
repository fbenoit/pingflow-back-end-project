const express = require('express');
const router = express.Router();
const starshipwarController = require('../controllers/StarshipwarController');
/* GET home page. */

router.get('/', starshipwarController.initiate);

  
module.exports = router;