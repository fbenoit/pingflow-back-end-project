const express = require('express');
const router = express.Router(); 
const UserController = require('../controllers/UserController.js');

router.get('/:userName', UserController.get);
router.post('/', UserController.create);
router.put('/', UserController.update);
router.delete('/:userName', UserController.delete);

/* function testUser(username, done) {
   User.findOne({ where: {username: username} }).then(user => {
    done(user);
  });
  
} */


module.exports = router;