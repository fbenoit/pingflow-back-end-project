const AsyncPolling = require('async-polling');
const StarhipsGame = require('../watchers/starshipsWatcher');

const Starhip = {

     initiate: function (reqObject, ws) {
        
        AsyncPolling( async function (end) {
            let results = await StarhipsGame.play();
            results && ws.send(JSON.stringify(results)); 
            end();
        }, 3000).run();
    
        
    }
}

module.exports= Starhip;