const UserPersistence = require('../persistence/models/UserPersistence');




const UserController = {
    create: function (req, res) {
        if (!req.body.userName || !req.body.password) {
            return res.json({
                message: 'missing parameter'
            })
        }

        UserPersistence.create({
            userName: req.body.userName,
            password: req.body.password
        }, function (err, user, created) {
            if(err){
                return res.json(
                    {
                        message: 'create failure',
                        error: true,
                        user: {}
                        })
            }
             
            return res.json(
                {
                    message: 'create success',
                     error: false,
                     user: {
                         userName: user.userName,
                         firstName: user.firstName,
                         lastName: user.lastName
                     }
                    })
        })
    },
    delete: function (req, res){
        if(!req.params.userName){
            return res.json({
                message: 'missing parameter'
            })
        }

        UserPersistence.delete({
            userName: req.params.userName
        }, function (err, affectedRows) {
            if(err){
                return res.json({
                    message: 'deleted fail',
                    err: err,
                    affectedRows: 0
                })
            }

            return res.json({
                message: affectedRows ? 'delete succes' : 'delete fail',
                err: false,
                affectedRows: affectedRows
            })
        })
    },
    get: function (req, res) {
        console.log(req.params.userName);
        if (!req.params.userName) {
            return res.json({
                message: 'missing parameter'
            })
        }

        UserPersistence.get({
            userName: req.params.userName
        }, function (err, user) {
            
            if(err || !user){
                return res.json({
                  err: true,
                  user: {}  
                })
            }
          
            return res.json({
                err: false,
                user: {
                    userName: req.params.userName,
                    firstName: user.firstName,
                    lastName: user.lastName
                }
            })
        })
    },
    update: function (req,res) {
        if (!req.body.userName) {
            return res.json({
                message: 'missing parameter'
            })
        }

        UserPersistence.update({
         userName: req.body.userName   
        },{
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            password: req.body.password
        }, function(err, user){

            if(err){
                return res.json({
                    message: 'update fail',
                    err: true
                })
            }

            return res.json({
                message: 'update success',
                err: false
            })
        })
    }
}


module.exports =  UserController; 