const AsyncPolling = require('async-polling');
const starshipConfig = require('../config/starshipConfig');
const axios = require('axios');
const valuesToCompare = [
    'cost', 'length', 'crew', 'passengers'
];
const StarhipsGame = {};

StarhipsGame.play = async function () {
    let results = await this.compareStarhips(); 
    return results;
}

StarhipsGame.starhipsDispatcher =  async function  () {
    const firstPageCall = randomizer(5, 1);
    const secondPageCall = randomizer(10,6);
    const firstSquadron = await axios.get(starshipConfig.baseUrl + '?page' + firstPageCall);
    const secondSquadron = await axios.get(starshipConfig.baseUrl + '?page' + secondPageCall);
    let starships = null;
  
    if(firstSquadron.data.results[secondPageCall] 
        && secondSquadron.data.results[firstPageCall])
        {
            starships = extractShips(firstSquadron.data.results[secondPageCall]
                                ,secondSquadron.data.results[firstPageCall] 
                                );
         }                            
    return starships;
}

StarhipsGame.compareStarhips =  async function () {
    
    let starhips = await(this.starhipsDispatcher());
    let results = {opposants:starhips};
    
    if(!starhips){
        return null;
    }
    
    const chosenValue = valuesToCompare[randomizer(3,1)];
    
    if(parseInt(starhips.first.chosenValue) === parseInt(starhips.second.chosenValue)){
        return results;
    }

    results.winner = parseInt(starhips.first.chosenValue) > parseInt(starhips.second.chosenValue) 
                        ? starhips.first.name 
                        : starhips.second.name;
    results.by = chosenValue;
    
    return results;
    
}

//TODO sort them 

randomizer = function (max, min) {
    return Math.floor(Math.random()*(max-min+1)+min)
}

extractShips = function (fisrSquadron, secondSquadron) {
    const starships = {
        first: {
           name: fisrSquadron.name,
           cost: fisrSquadron.cost_in_credits,
           length: fisrSquadron.length,
           passengers: fisrSquadron.passengers
        },
        second: {
            name: secondSquadron.name,
            cost: secondSquadron.cost_in_credits,
            length: secondSquadron.length,
            passengers: secondSquadron.passengers
        }
    }

    return starships;
}

module.exports= StarhipsGame;