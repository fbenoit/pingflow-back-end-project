const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const WebSocket = require('ws');
const AsyncPolling = require('async-polling');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const starshipswarRouter = require('./routes/starshipswar');
const loginRouter = require('./routes/login');
const signupRouter = require('./routes/signup');

const StarshipsController = require('./controllers/StarshipwarController');

const app = express();


const wss = new WebSocket.Server({ port: 8181 });

wss.on('connection', function connection(ws, req) {
    StarshipsController.initiate('initiate connection', ws);
});


app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.options('*', cors());

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/starshipswar', starshipswarRouter);
app.use('/login', loginRouter);
app.use('/signup', signupRouter);


module.exports = app;