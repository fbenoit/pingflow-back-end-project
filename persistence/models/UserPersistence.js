const User = require('./User');


const Persistence = {
    create: function (userObject, done) {
        console.log(userObject);
        User.findOrCreate({
            where: {
               userName: userObject.userName     
            }, 
            defaults: {
                password: userObject.password
            }
        }).spread(function(user, created){
            done(null, user.get({
                plain: true
              }), created);
        }).catch(function(err){
               done(err.errors, {}); 
        })
    },
    delete: function(userObject, done){
        User.destroy({
            where: {
                userName: userObject.userName
            }
        }).then(function (affectedRows) {
            done(null, affectedRows);
        }).catch(function (err) {
            done(err, '');
        })
    },
    get: function (userObject, done) {
        User.findOne({
            where:{
                userName: userObject.userName
            }
        }).then(function (user) {
            done(null, user);
        }).catch(function (err) {
            done(err, {})
        });
    },
    update: function (userObject, newUserValues, done) {
        User.findOne({
            where: {
                userName: userObject.userName
            }
        }).then(function (user) {
            if(!user){
                return done('err', {});
            }

            user.update({
                firstName: newUserValues.firstName,
                lastName: newUserValues.lastName,
                password: newUserValues.password
            }).then(function(user){
                done(null, user);
            }).catch(function (err) {
                done(err, {});
            })
        })
    } 
}

module.exports= Persistence;