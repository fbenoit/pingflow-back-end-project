const Sequelize = require('sequelize');
const Sequelizer = require('../sequelizer');

//définition du modèle de

var userDefinition = {
  userName: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true
  },
  firstName: {
    type: Sequelize.STRING,
  },
  lastName: {
    type: Sequelize.STRING,
  },
  password: {
    type: Sequelize.STRING,
  },
  salt: {
    type: Sequelize.STRING
  }
}
 
var User = Sequelizer.define('users',userDefinition);

module.exports = User;

 