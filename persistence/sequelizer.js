const Sequelize = require('sequelize');
const path = require('path');
const sequelize = new Sequelize(
    'sqlite:' + path.join(__dirname, 'ping.db'), 
    {operatorsAliases: false}
    );

module.exports = sequelize;


 //test de la connexion
sequelize
  .authenticate()
  .then(() => {
    console.log('Connexion réussie.');
  })
  .catch(err => {
    console.error('pas de connexion:', err);
  });
 